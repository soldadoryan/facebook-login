<html>
<head>
  <meta charset="utf-8">
  <title>Hubimi 1.0</title>
  <link rel="stylesheet" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
  <script src="https://use.fontawesome.com/88f58b9255.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="fb-scripts.js"></script>

</head>
<body>
  <h2 id="message">Bem vindo <span id="name"></span>!</h2>

  <ul id="dados" style="display: none">
    <li><b>Nome Completo:</b> <span id="nome_completo"></span></li>
    <li><b>Email:</b> <span id="email"></span></li>
    <li><b>Sexo:</b> <span id="sexo"></span></li>
    <li><b>Idade:</b> <span id="idade"></span></li>
    <li><b>Lingua:</b> <span id="lingua"></span></li>
    <div id="foto"></div>
  </ul><br>
  <a class="btn-facebook-login" style="display: none" href="#" id="login"><i class="fa fa-facebook"></i> Entrar com Facebook</a>
  <a class="btn-facebook-login" style="display: none" href="#" id="logout">Sair</a>
</body>
</html>
