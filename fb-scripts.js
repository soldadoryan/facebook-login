// inicializando SDK
(function(d, s, id){
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

//INICIALIZANDO FB API
window.fbAsyncInit = function() {
  FB.init({
    appId      : '1931344437105956',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.8'
  });
  FB.AppEvents.logPageView();

  FB.getLoginStatus(function(response) {
    VerificarLogin(response);
  });

  function VerificarLogin(response) {
    if(response.status == 'connected') {
      FB.api('/me', {fields: 'name, email, gender, picture, age_range, cover, locale'}, function(response) {
        $("#name").text(response.name);
        $("#nome_completo").text(response.name);
        $("#email").text(response.email);
        if(response.gender == 'male') {
          $("#sexo").text("Masculino");
        } else {
          $("#sexo").text("Feminino");
        }
        $("#idade").text(response.age_range.min + " - " + response.age_range.max);
        $("#lingua").text(response.locale);
        $("#logout").show();
        $("#login").hide();
        $("#dados").show();
      });

      FB.api('/me/picture', function(response) {
        $("#foto").html("<img style='width: 150px; margin-bottom: 25px; margin-top: 25px;' src='" + response.data.url +"'/>");
      });
    }
    else {
      $("#login").show();
      $("#logout").hide();
      $("#dados").hide();
      $("#name").text("");
    }
  }

  $("#login").click(function() {
    FB.login(function(response)
    {
      VerificarLogin(response);
    }, {scope: 'public_profile,email'});
  });

  $("#logout").click(function() {
    FB.logout(function(response) {
      VerificarLogin(response);
    });
  });
}
